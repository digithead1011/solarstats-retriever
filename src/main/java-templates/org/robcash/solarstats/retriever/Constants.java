/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

/**
 * Constants.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public final class Constants
{
	/** App version. */
	public static final String APP_VERSION = "${project.version}";

	/** AWS Context. */
	public static final String AWS_CONTEXT = "aws-context";

	/** Value for exception handling validation. */
	public static final String EXCEPTION_VALIDATION = "exception";

	/** Value for retriever validation. */
	public static final String RETRIEVER_VALIDATION = "retriever";

	/** Validation key. */
	public static final String VALIDATION = "validation";

	/** Prevent instantiation. */
	private Constants()
	{
		super();
	}
}
