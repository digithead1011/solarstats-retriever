openapi: 3.0.0
info:
  title: 'Monitoring API'
  description: 'Enphase Energy Monitoring API'
  contact:
    name: Enphase Energy
    url: 'https://developer-v4.enphase.com/'
    email: 'api@enphaseenergy.com'
  version: 'v4'
servers:
  - url: 'https://api-qa2.enphaseenergy.com'
paths:
  /api/v4/systems:
    parameters:
      - name: key
        in: query
        description: 'API key'
        required: true
    get:
      summary: 'Fetch systems'
      description: 'Returns a list of systems for which the user can make API requests. By default, systems are returned in batches of 10. The maximum size is 100.'
      tags:
        - 'System Details'
      parameters:
        - name: page
          in: query
          description: 'The page to be returned. Default=1, Min=1. For example, if page is set to 2, 2nd page is returned.'
        - name: size
          in: query
          description: 'Maximum number of records shown per page. Default=10, Min=1, Max=100.  For example, if max is set to 5, 5 records are shown per page.'
        - name: sort_by
          in: query
          description: 'Returns list of systems sorted by <sort_by> field. To get ASC order sorted list, user sort_by = <key>. To get DESC order sorted list, use sort_by = (-)<key>. Current sort keys supported are id, name, and last_report_date. By default the list is sorted by decreasing system ID.'
      responses:
        '200':
          description: 'List of Systems'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SystemsList'

  /api/v4/systems/{id}:
    get:
      summary: 'Retrieves a System by ID'
      description: 'Retrieves a System by ID'
      tags:
        - 'System Details'
      parameters:
        - name: id
          in: path
          required: true
          description: 'The unique numeric ID of the system. If an empty value is passed in the ID, this endpoint behaves as Fetch systems endpoint.'
      responses:
        '200':
          description: 'System fetched'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SystemSummary'

  /api/v4/systems/{system_id}/production_meter_readings:
    get:
      summary: 'Retrieve production meter readings'
      description: "Returns the last known reading of each production meter on the system as of the requested time, regardless of whether the meter is currently in service or retired. Read_at is the time at which the reading was taken, and is always less than or equal to the requested end_at. Commonly, the reading will be within 30 minutes of the requested end_at. However, larger deltas can occur and do not necessarily mean there is a problem with the meter or the system. Systems that are configured to report infrequently can show large deltas on all meters, especially when end_at is close to the current time. Meters that have been retired from a system will show an end_at that doesn't change, and that eventually is far away from the current time."
      tags:
        - 'Site Level Production Monitoring'
      parameters:
        - name: system_id
          in: path
          required: true
          description: 'The unique numeric ID of the system'
        - name: end_at
          in: query
          description: "End of reporting period in Unix epoch time. If no end is specified, defaults to the time of the request. If the end is later than the last reported interval the response data ends with the last reported interval."
      responses:
        '200':
          description: 'Production meter readings'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductionData'

  /api/v4/systems/{system_id}/energy_lifetime:
    get:
      summary: 'energy_lifetime'
      description: "Returns a daily time series of energy produced by the system over its lifetime. All measurements are in Watt hours.\n The time series includes one entry for each day from the start_date to the end_date with no gaps in the time series. If the response includes trailing zeroes, such as [909, 4970, 0, 0, 0], then no energy has been reported for the last days in the series.\n If the system has a meter, the time series includes data as measured by the microinverters until the first full day after the meter has been installed. Later, it switches to using the data as measured by the meter. This is called the “merged time series.”  The attribute “meter_start_date” indicates the time when the meter measurements begin to be used. You can retrieve the complete time series from the meter and from the microinverters by adding the parameter production=all to the request."
      tags:
        - 'Site Level Production Monitoring'
      parameters:
        - name: system_id
          in: path
          required: true
          description: 'The unique numeric ID of the system'
        - name: end_at
          in: query
          description: "End of reporting period in Unix epoch time. If no end is specified, defaults to the time of the request. If the end is later than the last reported interval the response data ends with the last reported interval."
        - name: start_date
          in: query
          description: "Start date of the time series data. Defaults to the system's operational date. If the start_date is earlier than the system's operational_date, then the response data begins with the system operational_date as start_date. Pass as String date format YYYY-MM-DD."
        - name: end_date
          in: query
          description: "End date of the time series data. Defaults to yesterday. If the end_date is later than yesterday, then the response data ends with yesterday as end_date. Pass as String date format YYYY-MM-DD."
        - name: production
          in: query
          description: 'When "all", returns the merged time series plus the time series as reported by the microinverters and the meter on the system. Other values are ignored.'
      responses:
        '200':
          description: 'When the query parameters include "production=all", returns meter and microinverter-measured time series:'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EnergyMetrics'

components:
  schemas:
    SystemsList:
      type: object
      properties:
        total:
          type: integer
          description: 'Total number of systmes'
        current_page:
          type: integer
          description: 'Number of the current page fetched'
        size:
          type: integer
          description: 'Maximum number of records shown per page. Default=10, Min=1, Max=100.'
        count:
          type: integer
          description: 'Total number of systems actually returned for the current page'
        items:
          type: string
          description: 'Named key of the list data. In this endpoint, it is systems'
        systems:
          type: array
          items:
            $ref: '#/components/schemas/SystemSummary'

    SystemSummary:
      type: object
      description: 'Summary information about a System'
      properties:
        system_id:
          type: integer
          description: 'The unique numeric ID of the system'
        name:
          type: string
          description: 'Name of the system'
        public_name:
          type: string
          description: 'Name displayed on the public system page. Available values are All, Residential System, Commercial etc. Default="Residential System". Only for systems that allow public access.'
        timezone:
          type: string
          description: 'Timezone to which the system belongs'
        address:
          type: object
          $ref: '#/components/schemas/Address'
        connection_type:
          type: string
          description: 'Connection Type of the system. Available values are ethernet, Wi-Fi, Cellular'
        status:
          type: string
          description: 'Status of the system. Corresponding Enlighten values can be found in table added at the end of https://developer-v4.enphase.com/docs.'
        last_report_at:
          type: integer
          description: "Timestamp (in epoch format) at which the system's Envoy last submitted a report."
        last_energy_at:
          type: integer
          description: "Timestamp (in epoch format) at which the system's produced energy was last reported. Even if the last produced energy is 0, its timestamp will be returned."
        operational_at:
          type: integer
          description: "Timestamp (in epoch format) at which this system became operational. Corresponds to the system's interconnect time, if one is specified. Otherwise, it is the system's first reported interval end time."
        attachment_type:
          type: string
          description: 'Micro inverter attachment type. Available values are rack_mount, zep, acm, bipv, frame_mount, railless_mount'
        interconnect_date:
          type: string
          description: 'Date on which the system was approved to connect to the grid'

    Address:
      type: object
      description: 'Address, for example, the physical location of a system'
      properties:
        state:
          type: string
        country:
          type: string
        postal_code:
          type: string

    ProductionData:
      type: object
      properties:
        system_id:
          type: integer
        meter_readings:
          type: array
          items:
            $ref: '#/components/schemas/MeterReading'
        meta:
          type: object
          $ref: '#/components/schemas/OperationalMetaData'

    MeterReading:
      type: object
      properties:
        serial_num:
          type: string
        value:
          type: integer
        read_at:
          type: integer

    EnergyMetrics:
      type: object
      properties:
        system_id:
          type: integer
        start_date:
          type: string
          format: date
        meter_start_date:
          type: string
          format: date
        production:
          type: array
          items:
            type: integer
        meta:
          type: object
          $ref: '#/components/schemas/OperationalMetaData'

    OperationalMetaData:
      type: object
      properties:
        status:
          type: string
        last_report_at:
          type: integer
        last_energy_at:
          type: integer
        operational_at:
          type: integer
