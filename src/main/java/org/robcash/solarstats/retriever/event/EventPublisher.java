/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.event;

import java.util.Map;

import org.robcash.solarstats.monitoring.model.EnergyMetrics;

import reactor.core.publisher.Mono;

/**
 * Publish energy metrics events downstream.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public interface EventPublisher
{

	/**
	 * Publish energy metrics events downstream.
	 *
	 * @param argMetrics Energy metrics.
	 */
	Mono<Void> publishEnergyMetrics(EnergyMetrics argMetrics);

	/**
	 * Publish energy metrics events downstream.
	 *
	 * @param argMetrics Energy metrics.
	 * @param argMetadata Metadata to be included in the event that is published.
	 */
	Mono<Void> publishEnergyMetrics(EnergyMetrics argMetrics, Map<String, Object> argMetadata);

}
