/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.event;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.robcash.solarstats.event.EnergyMetricsEvent;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.robcash.solarstats.monitoring.model.SolarProductionData;
import org.robcash.solarstats.retriever.event.config.SNSConfigProperties;
import org.robcash.utils.tracing.annotation.Traced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.sns.SnsAsyncClient;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishResponse;

/**
 * Event publisher that publishes events to SNS.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class SNSEventPublisher implements EventPublisher
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(SNSEventPublisher.class);

	/** SNS String data type. */
	private static final String STRING_DATA_TYPE = "String";

	/** SNS client. */
	private final SnsAsyncClient client;

	/** Configuration. */
	private final SNSConfigProperties config;

	/** Object Mapper. */
	@Autowired
	private ObjectMapper mapper;

	/**
	 * Create a new {@link EventPublisher} that sends events to an SNS topic.
	 *
	 * @param argClient SNS client.
	 * @param argConfig Configuration.
	 */
	public SNSEventPublisher(final SnsAsyncClient argClient, final SNSConfigProperties argConfig)
	{
		this.client = argClient;
		this.config = argConfig;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Traced("Publish Events")
	public Mono<Void> publishEnergyMetrics(final EnergyMetrics argMetrics)
	{
		return publishEnergyMetrics(argMetrics, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Traced("Publish Events")
	public Mono<Void> publishEnergyMetrics(final EnergyMetrics argMetrics, final Map<String, Object> argMetadata)
	{
		Assert.notNull(argMetrics, "The metrics to publish cannot be null");

		LOG.info("Publishing energy metrics event to SNS topic {}", this.config.getTopicName());

		// Create event
		final SolarProductionData data = argMetrics.getSolarProduction();
		final EnergyMetricsEvent event = EnergyMetricsEvent.newStatsRetrieved(argMetrics);
		if (argMetadata != null)
		{
			event.setMetadata(argMetadata);
		}

		try
		{
			// Create message
			final String message = this.mapper.writeValueAsString(event);
			final Map<String, MessageAttributeValue> attributes = new HashMap<>();

			// So why put the event type as metadata?
			// Well, SNS filtering can only be done on messages attributes and there has to be at least one
			// attribute in order for it to work correctly. If the message has no attributes and there's a
			// filter criteria that matches the lack of an attribute, then surprisingly that doesn't match at all.
			// In order to match the lack of an attribute, there has to be one other attribute present. This
			// is intended to be that attribute.
			attributes.put("event", MessageAttributeValue.builder()
					.dataType(STRING_DATA_TYPE)
					.stringValue(event.getEventType().getValue())
					.build());

			// Add any other attributes provided in the metadata
			if (argMetadata != null)
			{
				argMetadata.entrySet()
						.stream()
						.filter(entry -> String.class.isInstance(entry.getValue()))
						.forEach(entry -> {
							attributes.put(entry.getKey(), MessageAttributeValue.builder()
									.dataType(STRING_DATA_TYPE)
									.stringValue((String) entry.getValue())
									.build());
						});
			}

			// Log
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Message to publish: {}", message);
				LOG.debug("Message attributes: {}", attributes.entrySet()
						.stream()
						.map(entry -> {
							return entry.getKey() + " = " + entry.getValue().toString();
						})
						.collect(Collectors.joining("; ")));
			}

			// Publish message
			final CompletableFuture<PublishResponse> result = this.client.publish(req -> {
				req.topicArn(this.config.getTopicArn())
						.subject(event.getEventType().getValue())
						.message(message)
						.messageAttributes(attributes);
			});

			// Handle response
			return Mono.fromFuture(result)
					.doOnNext(response -> {
						// No error - whew!
						if (LOG.isDebugEnabled())
						{
							LOG.debug("Successfully published message with metrics for {} belonging to customer {} to "
									+ "SNS topic {}: messageId is {}", data.getSystemName(), argMetrics.getCustomerId(),
									this.config.getTopicName(), response.messageId());
						}
					})
					.doOnError(t -> {
						LOG.error("Failed to publish message with metrics for {} belong to customer {} to SNS topic {}",
								data.getSystemName(), argMetrics.getCustomerId(), this.config.getTopicName(), t);
					})
					.then();
		}
		catch (final JsonProcessingException ex)
		{
			LOG.error("Failed to serialize EnergyMetricsEvent into JSON", ex);
			throw new RuntimeException("Failed to serialize EnergyMetricsEvent into JSON", ex);
		}
	}

}
