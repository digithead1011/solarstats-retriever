/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.event.config;

import org.robcash.solarstats.retriever.config.FunctionConfigProperties;
import org.robcash.solarstats.retriever.event.SNSEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import software.amazon.awssdk.services.sns.SnsAsyncClient;

/**
 * Spring beans for publishing events to SNS.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
public class SNSConfig
{
	@Autowired
	private FunctionConfigProperties config;

	@Bean
	@Profile("local")
	SnsAsyncClient localSNSClient()
	{
		return SnsAsyncClient.builder()
				.endpointOverride(this.config.getSns().getEndpoint())
				.build();
	}

	@Bean
	@ConditionalOnMissingBean(SnsAsyncClient.class)
	SnsAsyncClient awsSNSClient()
	{
		return SnsAsyncClient.create();
	}

	@Bean
	SNSEventPublisher snsPublisher(final SnsAsyncClient argClient)
	{
		return new SNSEventPublisher(argClient, this.config.getSns());
	}

}
