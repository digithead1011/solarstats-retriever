/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.event.config;

import java.net.URI;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for {@link SNSEventPublisher}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@ConfigurationProperties
public class SNSConfigProperties
{
	/** Endpoint. */
	private URI endpoint;

	/** Topic name. */
	private String topicName;

	/** Topic ARN. */
	private String topicARN;

	/**
	 * Create new instance of SNSPublisherConfigProperties.
	 */
	public SNSConfigProperties()
	{
		super();
	}

	/**
	 * Get the service endpoint.
	 *
	 * @return Service endpoint.
	 */
	public URI getEndpoint()
	{
		return this.endpoint;
	}

	/**
	 * Set the service endpoint.
	 *
	 * @param argEndpoint Service endpoint.
	 */
	public void setEndpoint(final URI argEndpoint)
	{
		this.endpoint = argEndpoint;
	}

	/**
	 * Get the topic name.
	 *
	 * @return Topic name.
	 */
	public String getTopicName()
	{
		return this.topicName;
	}

	/**
	 * Set the topic name.
	 *
	 * @param argTopicName Topic name.
	 */
	public void setTopicName(final String argTopicName)
	{
		this.topicName = argTopicName;
	}

	/**
	 * Get the topic ARN.
	 *
	 * @return Topic ARN.
	 */
	public String getTopicArn()
	{
		return this.topicARN;
	}

	/**
	 * Set the topic ARN.
	 *
	 * @param argTopicName Topic ARN.
	 */
	public void setTopicArn(final String argTopicARN)
	{
		this.topicARN = argTopicARN;
	}

}
