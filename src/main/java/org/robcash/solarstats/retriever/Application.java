/*
 * Copyright (C) 2020-2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Spring Boot application.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = false)
public class Application
{
	public static void main(final String[] args)
	{
		SpringApplication.run(Application.class, args);
	}

}
