/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

import java.time.LocalDate;
import java.util.function.Consumer;

import org.robcash.customer.ReactiveCustomerService;
import org.robcash.solarstats.retriever.event.EventPublisher;
import org.robcash.solarstats.retriever.monitoring.EnphaseMonitoringService;
import org.robcash.solarstats.retriever.monitoring.SenseMonitoringService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;

import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;

import reactor.core.publisher.Mono;

/**
 * Function that retrieves solar production statistics and generates events with results.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class RetrieveStatisticsFunction extends AbstractRetrieverFunction implements Consumer<Message<ScheduledEvent>>
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(RetrieveStatisticsFunction.class);

	/** Customer service retrieves information about customers. */
	@Autowired
	@Qualifier("cachingReactiveCustomerService")
	private ReactiveCustomerService customerService;

	/** Solar production monitoring service. */
	@Autowired
	private EnphaseMonitoringService productionService;

	/** Usage monitoring service. */
	@Autowired
	private SenseMonitoringService usageMonitor;

	/** Event broadcaster. */
	@Autowired
	private EventPublisher eventPublisher;

	/**
	 * Create new instance of RetrieveStatisticsFunction.
	 */
	public RetrieveStatisticsFunction()
	{
		super();
	}

	/**
	 * Responds to AWS scheduled events by retrieving solar statistics for supported customrers.
	 *
	 * @param Message containing AWS scheduled event, which contains the time it was triggered.
	 */
	@Override
	public void accept(final Message<ScheduledEvent> argEvent)
	{
		// Get payload from request message
		final ScheduledEvent trigger = argEvent.getPayload();

		if (argEvent.getHeaders().containsKey(Constants.EXCEPTION_VALIDATION))
		{
			final StringBuilder builder = new StringBuilder("Lambda error validation: ");
			builder.append(argEvent.getHeaders().containsKey(Constants.EXCEPTION_VALIDATION));
			throw new RuntimeException(builder.toString());
		}
		// Determine requested date and then process request for all customers
		final LocalDate statsAsOfDate = getRequestedAsOfDate(trigger);

		// Get customers
		LOG.info("Getting list of customers");
		this.customerService.getAllCustomers()
				// Log whether or not customer has fields required for SolarStats
				.doOnNext(c -> {
					LOG.debug("Customer {} {} configured for SolarStats", c.getId(),
							c.isSolarStatsReady() ? "is" : "is not");
				})

				// Remove customers who don't have all required fields
				.filter(c -> c.isSolarStatsReady())

				.flatMap(c -> {
					// Get solar production stats for each customer
					return this.productionService.getEnergyMetricsFor(c, statsAsOfDate)

							.flatMap(metrics -> {
								final String solarSystemId = metrics.getSolarProduction().getSystemId();
								if (c.hasUsageMonitorFor(solarSystemId))
								{
									// Retrieve energy usage
									final String usageMonitorId = c.getUsageMonitorFor(solarSystemId);
									return this.usageMonitor.getEnergyUsageFor(c, usageMonitorId, statsAsOfDate)
											.map(usage -> metrics.usage(usage));
								}
								else
								{
									return Mono.just(metrics);
								}
							});
				})

				// Publish each set of production data to EventBridge
				.flatMap(this.eventPublisher::publishEnergyMetrics)

				// Block to let publishing finish. Normally we wouldn't want to block, but this is the last thing
				// to do before returning from the Lambda function.
				.blockLast();
	}

}
