/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import org.robcash.solarstats.retriever.config.FunctionConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;

/**
 * Abstract stats retriever.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class AbstractRetrieverFunction
{

	/** Configuration. */
	@Autowired
	protected FunctionConfigProperties config;

	/**
	 * Create new instance of AbstractRetrieverFunction.
	 */
	public AbstractRetrieverFunction()
	{
		super();
	}

	/**
	 * Determine the date for which the solar stats should be retrieved. This is always the date the event was triggered
	 * minus the number of days specified by the date offset property.
	 *
	 * @param argEvent AWS Scheduled event.
	 * @return Zoned date time representing the date as of when solar statistics should be retrived.
	 */
	protected LocalDate getRequestedAsOfDate(final ScheduledEvent argEvent)
	{
		// AWS reports timestamps using Joda time, but we want to work with Java8 time.
		// Convert Joda to Java8 using milliseconds since the epoch as the converstion point.
		final long millis = argEvent.getTime().getMillis();
		LocalDate convertedDate = LocalDate.ofInstant(Instant.ofEpochMilli(millis), ZoneId.of("UTC"));
		if (Math.abs(this.config.getDateOffset()) > 0)
		{
			convertedDate = convertedDate.minusDays(Math.abs(this.config.getDateOffset()));
		}

		return convertedDate;
	}

}