/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring.config;

import org.robcash.enphase.v4.ReactiveEnphaseService;
import org.robcash.solarstats.retriever.monitoring.EnphaseMonitoringService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * SpringBoot configuration for Enphase Energy.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
public class EnphaseEnergyConfig
{
	@Bean
	public EnphaseMonitoringService enphaseMonitoringService(final ReactiveEnphaseService reactivEnphaseService)
	{
		return new EnphaseMonitoringService(reactivEnphaseService);
	}

}
