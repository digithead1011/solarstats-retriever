/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring.config;

import org.robcash.solarstats.retriever.monitoring.SenseMonitoringService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.handler.logging.LogLevel;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

/**
 * Spring configuration for Sense monitor.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
public class SenseConfig
{

	@Bean
	WebClient senseRestApiClient()
	{
		WebClient.Builder builder = WebClient.builder()
				.baseUrl("https://api.sense.com");

		if (true) // this.config.isTraceLoggingEnabled())
		{
			// Enable wiretapping on HttpClient
			final HttpClient httpClient = HttpClient.create()
					.wiretap("org.robcash.solarstats.retriever.monitoring.sense", LogLevel.TRACE,
							AdvancedByteBufFormat.TEXTUAL);
			final ClientHttpConnector conn = new ReactorClientHttpConnector(httpClient);
			builder = builder.clientConnector(conn);
		}

		return builder.build();
	}

	@Bean
	SenseMonitoringService senseMonitor(@Qualifier("senseRestApiClient") final WebClient argApiClient)
	{
		return new SenseMonitoringService(argApiClient);
	}

}
