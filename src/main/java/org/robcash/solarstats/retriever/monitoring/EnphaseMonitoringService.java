/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import org.robcash.customer.model.Customer;
import org.robcash.enphase.v4.ReactiveEnphaseService;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.robcash.solarstats.monitoring.model.SolarProductionData;
import org.robcash.utils.tracing.annotation.Traced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enphaseenergy.api.v4.monitoring.model.SystemSummary;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service for interacting with Enphase Energy to retrieve solar production statistics.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class EnphaseMonitoringService implements EnergyMonitoringService
{
	/** Enphase provider. */
	public static final String ENPHASE = "Enphase Energy";

	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(EnphaseMonitoringService.class);

	/** Enphase client. */
	private final ReactiveEnphaseService enphaseClient;

	/**
	 * Create new instance of EnphaseMonitoringService.
	 *
	 * @param argEnphaseClient Enphase API client.
	 */
	public EnphaseMonitoringService(final ReactiveEnphaseService argEnphaseClient)
	{
		super();
		this.enphaseClient = argEnphaseClient;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDataProviderName()
	{
		return ENPHASE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Traced("Retrieve Stats for Customer")
	public Flux<EnergyMetrics> getEnergyMetricsFor(final Customer argCustomer,
			final LocalDate argAsOfDate)
	{
		LOG.info("Requesting production stats for {} from {} on {}", argCustomer.getName(),
				getDataProviderName(),
				argAsOfDate.format(DateTimeFormatter.ISO_LOCAL_DATE));

		return this.enphaseClient.retrieveSystemsListFor(argCustomer.getId())
				.doOnNext(token -> {
					if (LOG.isDebugEnabled())
					{
						LOG.debug("Calling Enphase to retrieve the list of systems for {}", argCustomer.getName());
					}
				})

				// Transform list of systems into a Flux of system summaries
				.flatMapMany(systemList -> {
					return Flux.fromIterable(systemList.getSystems());
				})

				// Transform each system summary into system production data
				.flatMap(systemSummary -> {
					return this.fetchSystemProductionDataFor(systemSummary, argCustomer, argAsOfDate);
				});
	}

	/**
	 * Fetch system production data for a specific system on a specific date.
	 *
	 * @param argSystem System for which production data is being retrieved.
	 * @param argCustomer Customer for whom production data is being retrieved.
	 * @param argAsOfDate Date on which production data was generated.
	 * @return Solar production data.
	 */
	private Mono<EnergyMetrics> fetchSystemProductionDataFor(
			final SystemSummary argSystem, final Customer argCustomer, final LocalDate argAsOfDate)
	{
		// The As of date is the date where this application is running, minus any offset days.
		// If the current time where the system (solar array) is located is before end of day on the as of day, then
		// the reporting period hasn't ended yet and we need to look at a prior day.

		// Get date and time where system is located
		final ZoneId systemZone = ZoneId.of(argSystem.getTimezone());
		final ZonedDateTime currentSystemTime = ZonedDateTime.now(systemZone);

		// Get as of date in timezone where system is located
		ZonedDateTime asOfTS = ZonedDateTime.of(argAsOfDate, LocalTime.MAX, systemZone);

		while (currentSystemTime.isBefore(asOfTS))
		{
			// System time is before proposed end of period.
			// Energy production not done for that time period, so back up a day.
			asOfTS = asOfTS.minusDays(1);
		}
		final ZonedDateTime periodStartTS = asOfTS.truncatedTo(ChronoUnit.DAYS);
		final ZonedDateTime periodEndTS = ZonedDateTime.from(asOfTS);

		// Enphase allows API consumers to pass an end-of-period time expressed as time in millis since the Unix epoch
		final String periodStart = DateTimeFormatter.ISO_LOCAL_DATE.format(periodStartTS);
		final String periodEnd = DateTimeFormatter.ISO_LOCAL_DATE.format(periodEndTS);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Calling Enphase to retrieve production data for system {} as of {} at end of day",
					argSystem.getSystemId(), periodEnd);
		}

		return this.enphaseClient.retrieveEnergyMetricsFor(argCustomer.getId(), argSystem, periodStart, periodEnd)
				.map(enphaseData -> {
					// Extrace production data from enphase metrics
					return extractEnergyProducedFor(argSystem, enphaseData, periodEnd);
				})
				.map(energyProduced -> {
					// Create solar production data
					return createProductionDataFor(argSystem, argCustomer, periodStartTS, periodEndTS, energyProduced);
				})
				.doOnNext(metrics -> {
					// Log results
					final SolarProductionData data = metrics.getSolarProduction();
					LOG.info(
							"{} reported that system \"{}\" belonging to {} generated {} watt-hours of electricity "
									+ "during the period {} to {}",
							data.getDataSource(), data.getSystemName(), argCustomer.getName(), data.getWattHours(),
							data.getPeriodStart(), data.getPeriodEnd());
				});
	}

	/**
	 * Get the amount of energy produced by a system in Watt-Hours.
	 *
	 * @param argSystem System for which energy should be returned.
	 * @param argEnergyMetrics Energy production metrics for the system.
	 * @param argPeriodEnd Period end.
	 * @return Energy produced in WH.
	 */
	private Integer extractEnergyProducedFor(final SystemSummary argSystem,
			final com.enphaseenergy.api.v4.monitoring.model.EnergyMetrics argEnergyMetrics, final String argPeriodEnd)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Enphase system data: {}", argEnergyMetrics);
		}
		final List<Integer> rawProductionData = argEnergyMetrics.getProduction();
		Integer energyProduced = 0;
		if (rawProductionData == null || rawProductionData.isEmpty())
		{
			// Do something mean and nasty
			LOG.warn("Enphase report did not include production data for system {} as of {}",
					argSystem.getSystemId(), argPeriodEnd);
		}
		else
		{
			energyProduced = rawProductionData.get(0);
		}
		return energyProduced;
	}

	/**
	 * Create solar production data by combining system information, customer information, a date range, and the amount
	 * of energy that was produced.
	 *
	 * @param argSystem System.
	 * @param argCustomer Customer.
	 * @param argPeriodStart Period start.
	 * @param argPeriodEnd Period end.
	 * @param argEnergyProduced Energy produced.
	 * @return
	 */
	private EnergyMetrics createProductionDataFor(final SystemSummary argSystem,
			final Customer argCustomer,
			final ZonedDateTime argPeriodStart, final ZonedDateTime argPeriodEnd, final Integer argEnergyProduced)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		return new EnergyMetrics()
				.customerId(argCustomer.getId())
				.timestamp(Instant.now())
				.solarProduction(new SolarProductionData()
						.dataSource(ENPHASE)
						.systemId(argSystem.getSystemId().toString())
						.systemName(argSystem.getName())
						.periodStart(formatter.format(argPeriodStart))
						.periodEnd(formatter.format(argPeriodEnd))
						.wattHours(argEnergyProduced)
						.asOfDate(new Date()));
	}
}
