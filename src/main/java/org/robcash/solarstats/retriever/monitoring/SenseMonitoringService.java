/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.robcash.customer.model.Customer;
import org.robcash.solarstats.monitoring.model.EnergyUsage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

/**
 * Service for interacting with Sense to retrieve energy usage statistics.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class SenseMonitoringService
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(SenseMonitoringService.class);

	/** Sense provider. */
	public static final String SENSE = "Sense";

	/** Web Client. */
	private final WebClient apiClient;

	/** Object mapper. */
	@Autowired
	private ObjectMapper mapper;

	/**
	 * Create new instance of SenseMonitoringService.
	 */
	public SenseMonitoringService(final WebClient argApiClient)
	{
		super();
		this.apiClient = argApiClient;
	}

	/**
	 * Returns the name of the data provider.
	 *
	 * @return Name of the data provider.
	 */
	public String getDataProviderName()
	{
		return SENSE;
	}

	/**
	 * Get statistis from a usage monitor on the specified day.
	 *
	 * @param argCustomer Customer for whom the usage is being collected.
	 * @param argMonitorId Monitor from which to collect data.
	 * @param argAsOfDate Date as of when the usage should be collected.
	 */
	public Mono<EnergyUsage> getEnergyUsageFor(final Customer argCustomer, final String argMonitorId,
			final LocalDate argAsOfDate)
	{
		LOG.info("Requesting usage stats for {} from {} on {}", argCustomer.getName(),
				getDataProviderName(),
				argAsOfDate.format(DateTimeFormatter.ISO_LOCAL_DATE));

		final String systemTimezoneAsString = "America/New_York";
		// final String monitorId = "249049";
		// final String authToken = "t1.50685.50214.f69b82bb19bc14b70f24c48ef74cc527fa2759a453f04480c63a3025224847bf";

		// The As of date is the date where this application is running, minus any offset days.
		// If the current time where the system (solar array) is located is before end of day on the as of day, then
		// the reporting period hasn't ended yet and we need to look at a prior day.

		// Get date and time where system is located
		final ZoneId systemZone = ZoneId.of(systemTimezoneAsString);
		final ZonedDateTime currentSystemTime = ZonedDateTime.now(systemZone);

		// Get as of date in timezone where system is located
		ZonedDateTime asOfTS = ZonedDateTime.of(argAsOfDate, LocalTime.MAX, systemZone);

		while (currentSystemTime.isBefore(asOfTS))
		{
			// System time is before proposed end of period.
			// Energy production not done for that time period, so back up a day.
			asOfTS = asOfTS.minusDays(1);
		}
		final ZonedDateTime periodStartTS = asOfTS.truncatedTo(ChronoUnit.DAYS);

		// Enphase allows API consumers to pass an end-of-period time expressed as time in millis since the Unix epoch
		final String periodStart = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(periodStartTS);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Calling Sense to retrieve usage data for monitor {} as of {} at end of day",
					argMonitorId, periodStartTS.format(DateTimeFormatter.ISO_LOCAL_DATE));
		}

		return this.apiClient
				.get()
				.uri(builder -> builder.path("/apiservice/api/v1/app/history/trends")
						.queryParam("monitor_id", argMonitorId)
						.queryParam("scale", "DAY")
						.queryParam("start", periodStart)
						.build())
				.headers(h -> h.setBearerAuth(argCustomer.getSenseToken()))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.onStatus(HttpStatus::is5xxServerError, resp -> {
					LOG.error("Sense API failed with status code {}", resp.rawStatusCode());
					return resp.createException();
				})
				.bodyToMono(String.class)
				.retryWhen(Retry.backoff(3, Duration.ofSeconds(5))
						.filter(t -> t instanceof WebClientResponseException
								&& ((WebClientResponseException) t).getStatusCode().is5xxServerError()))
				.map(this::parseJson)
				.map(json -> createEnergyUsage(json, argMonitorId))
				.onErrorComplete();
	}

	private JsonNode parseJson(final String argJson)
	{
		try
		{
			return this.mapper.readTree(argJson);
		}
		catch (final JsonProcessingException ex)
		{
			throw new RuntimeException("Failed to read JSON response from Sense API call", ex);
		}
	}

	private EnergyUsage createEnergyUsage(final JsonNode json, final String argMonitorId)
	{
		return new EnergyUsage()
				.dataSource(SENSE)
				.monitorId(argMonitorId)
				.energyFromGrid(
						(int) Math.round(json.path("from_grid").asDouble(0) * 1000))
				.energyToGrid(
						(int) Math.round(json.path("to_grid").asDouble(0) * 1000))
				.asOfDate(new Date());
	}

}
