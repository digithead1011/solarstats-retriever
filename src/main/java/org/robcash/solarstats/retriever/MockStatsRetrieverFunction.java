/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.robcash.customer.ReactiveCustomerService;
import org.robcash.customer.model.Customer;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.robcash.solarstats.monitoring.model.EnergyUsage;
import org.robcash.solarstats.monitoring.model.SolarProductionData;
import org.robcash.solarstats.retriever.event.EventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;

import reactor.core.publisher.Mono;

/**
 * This function is invoked as part of validation activities. It will function in somewhat of a mock mode, predominantly
 * test connectivity.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class MockStatsRetrieverFunction extends AbstractRetrieverFunction implements Consumer<Message<ScheduledEvent>>
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(MockStatsRetrieverFunction.class);

	/** CodeDeploy metadata. */
	private static final String CODEDEPLOY_METADATA_KEY = "codedeploy";

	/** CodeDeploy deployment ID. */
	private static final String CODEDEPLOY_DEPLOYMENT_ID = "deploymentId";

	/** CodeDeploy execution ID. */
	private static final String CODEDEPLOY_EXECUTION_ID = "executionId";

	/** Mock customer ID. */
	private static final String MOCK_CUSTOMER_ID = "99999";

	/** Mock system ID. */
	private static final String MOCK_SYSTEM_ID = "MOCK";

	/** Mock system name. */
	private static final String MOCK_SYSTEM_NAME = "Mock System";

	/** Mock energy produced. */
	private static final Integer MOCK_ENERGY_PRODUCED = Integer.valueOf(1000);

	/** Mock data source. */
	private static final String MOCK_DATA_SOURCE = "Validator";

	/** Customer service retrieves information about customers. */
	@Autowired
	@Qualifier("cachingReactiveCustomerService")
	private ReactiveCustomerService customerService;

	/** Event broadcaster. */
	@Autowired
	private EventPublisher eventPublisher;

	/**
	 * Responds to AWS scheduled events by retrieving solar statistics for supported customrers.
	 *
	 * @param argMessage Message containing AWS scheduled event, which contains the time it was triggered.
	 */
	@Override
	public void accept(final Message<ScheduledEvent> argMessage)
	{
		// Sanity checks
		Assert.notNull(argMessage, "The event to process cannot be null");
		final Context awsContext = (Context) argMessage.getHeaders().get(Constants.AWS_CONTEXT);
		Assert.notNull(awsContext, "The AWS Context cannot be null");
		final String validationMode = (String) argMessage.getHeaders().getOrDefault(Constants.VALIDATION,
				Constants.RETRIEVER_VALIDATION);

		LOG.info("Running in validation mode: {}", validationMode);
		LOG.info("Validating functionality and connectivity of {}", awsContext.getFunctionName());

		// Get payload from request message
		final ScheduledEvent event = argMessage.getPayload();
		final Map<String, Object> detail = event.getDetail();

		// Gather metadata
		final Map<String, Object> metadata = new HashMap<>();
		metadata.put(Constants.VALIDATION, validationMode);
		if (detail != null)
		{
			final Map<String, Object> codedeploy = new HashMap<>();
			codedeploy.put(CODEDEPLOY_DEPLOYMENT_ID, detail.get(CODEDEPLOY_DEPLOYMENT_ID));
			codedeploy.put(CODEDEPLOY_EXECUTION_ID, detail.get(CODEDEPLOY_EXECUTION_ID));
			metadata.put(CODEDEPLOY_METADATA_KEY, codedeploy);
		}

		// Call Customer API
		this.customerService.getAllCustomers()
				// If empty, replace with a single customer
				.switchIfEmpty(Mono.just(new Customer()))

				// Select only the first customer
				.next()

				// Transform the customer into mock energy metrics
				.map(c -> {
					return createMockEnergyMetricsFor(MOCK_CUSTOMER_ID);
				})

				// Publish the metrics
				.flatMap(metrics -> {
					return this.eventPublisher.publishEnergyMetrics(metrics, metadata);
				})

				// Wait for publishing to finish
				.block();

		return;
	}

	/**
	 * Create mock energy metrics for a customer.
	 * 
	 * @param argCustomerId Customer ID.
	 * @return Mock energy metrics.
	 */
	private EnergyMetrics createMockEnergyMetricsFor(final String argCustomerId)
	{
		final OffsetDateTime start = OffsetDateTime.now(ZoneId.of("America/New_York"))
				.truncatedTo(ChronoUnit.DAYS);
		DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start);
		final OffsetDateTime end = start.plusDays(1).minusNanos(1);

		final EnergyMetrics mockMetrics = new EnergyMetrics()
				.customerId(argCustomerId)
				.solarProduction(new SolarProductionData()
						.asOfDate(new Date())
						.dataSource(MOCK_DATA_SOURCE)
						.periodStart(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(start))
						.periodEnd(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(end))
						.systemId(MOCK_SYSTEM_ID)
						.systemName(MOCK_SYSTEM_NAME)
						.wattHours(MOCK_ENERGY_PRODUCED))
				.timestamp(Instant.now())
				.usage(new EnergyUsage()
						.asOfDate(new Date())
						.dataSource(MOCK_DATA_SOURCE)
						.energyFromGrid(1000)
						.energyToGrid(1000));

		return mockMetrics;
	}
}
