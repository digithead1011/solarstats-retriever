/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.config;

import org.robcash.solarstats.retriever.event.config.SNSConfigProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for the RetrieveStatisticsFunction.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@ConfigurationProperties(prefix = "solarstats.retriever")
public class FunctionConfigProperties
{
	/** Offset days. */
	private int dateOffsetInDays = 0;

	/** Whether or not to dump the environment variables. */
	private boolean debugEnvironment = false;

	/** SNS config. */
	private SNSConfigProperties snsConfig;

	/**
	 * Create new instance of FunctionConfigProperties.
	 */
	public FunctionConfigProperties()
	{
		super();
		this.snsConfig = new SNSConfigProperties();
	}

	/**
	 * Get the number of days to offset the request date.
	 *
	 * @return Number of days to offset the request date.
	 */
	public int getDateOffset()
	{
		return this.dateOffsetInDays;
	}

	/**
	 * Set the number of days to offset the request date.
	 *
	 * @param argOffset Number of days to offset the request date.
	 */
	public void setDateOffset(final int argOffset)
	{
		this.dateOffsetInDays = argOffset;
	}

	/**
	 * Get the debugEnvironment flag.
	 *
	 * @return {@code true} if the environment should be logged.
	 */
	public boolean isDebugEnvironment()
	{
		return this.debugEnvironment;
	}

	/**
	 * Set the debugEnvironment flag.
	 *
	 * @param argDebugEnvironment Debug flag.
	 */
	public void setDebugEnvironment(final boolean argDebugEnvironment)
	{
		this.debugEnvironment = argDebugEnvironment;
	}

	/**
	 * Get the SNS configuration properties.
	 *
	 * @return SNS configuration properties.
	 */
	public SNSConfigProperties getSns()
	{
		return this.snsConfig;
	}

	/**
	 * Set the SNS configuration properties.
	 *
	 * @param argConfig SNS configuration properties.
	 */
	public void setSns(final SNSConfigProperties argConfig)
	{
		this.snsConfig = argConfig;
	}
}
