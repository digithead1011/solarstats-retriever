/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.config;

import java.util.function.Consumer;

import org.robcash.solarstats.retriever.RetrieveStatisticsFunction;
import org.robcash.solarstats.retriever.MockStatsRetrieverFunction;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;

/**
 * Spring bean configuration for functions.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(FunctionConfigProperties.class)
public class FunctionConfig
{
	public static final String RETRIEVER_BEAN = "solarStatsRetriever";

	public static final String MOCK_BEAN = "mockSolarStatsRetriever";

	@Bean
	public Consumer<Message<ScheduledEvent>> solarStatsRetriever()
	{
		return new RetrieveStatisticsFunction();
	}

	@Bean
	public Consumer<Message<ScheduledEvent>> mockSolarStatsRetriever()
	{
		return new MockStatsRetrieverFunction();
	}

}
