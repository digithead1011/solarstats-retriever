/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.robcash.solarstats.retriever.config.FunctionConfig;
import org.robcash.solarstats.retriever.config.FunctionConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.ObjectUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;

/**
 * AWS Lambda request handler. This class is what is called by Lambda and is reponsible for routing the incoming event
 * to the correct function.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class SolarStatsRetrieverRequestHandler implements RequestHandler<ScheduledEvent, Void>, AutoCloseable
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(SolarStatsRetrieverRequestHandler.class);

	final ConfigurableApplicationContext springContext;

	public SolarStatsRetrieverRequestHandler()
	{
		final String[] args = new String[] { "--spring.cloud.function.web.export.enabled=false",
				"--spring.main.web-application-type=none" };
		this.springContext = SpringApplication.run(Application.class, args);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Void handleRequest(final ScheduledEvent argInput, final Context argContext)
	{
		LOG.info("SolarStatsRetriever {} starting", getAppVersion(argContext));

		if (LOG.isDebugEnabled())
		{
			LOG.info("Received ScheduledEvent: {}", argInput);
		}

		try
		{
			// Get the configuration
			final FunctionConfigProperties config = this.springContext.getBean(FunctionConfigProperties.class);

			// Dump environment variables
			if (config.isDebugEnvironment() && LOG.isDebugEnabled())
			{
				final StringBuilder sBuilder = new StringBuilder();
				sBuilder.append("Environment Variables:\n");
				final Map<String, String> environment = System.getenv();
				environment.keySet().stream().sorted().forEach(key -> {
					sBuilder.append(key);
					sBuilder.append(" = ");
					sBuilder.append(environment.get(key));
					sBuilder.append("\n");
				});
				LOG.debug(sBuilder.toString());
			}

			// Prepare Message
			final Map<String, Object> messageHeaders = new HashMap<>();
			messageHeaders.put(Constants.AWS_CONTEXT, argContext);

			// Determine if the function should operate in regular mode or validation mode
			final Map<String, Object> detail = argInput.getDetail();

			// Get the appropriate function
			Consumer<Message<ScheduledEvent>> function = null;
			if (detail == null || ObjectUtils.isEmpty(detail.get(Constants.VALIDATION)))
			{
				function = (Consumer<Message<ScheduledEvent>>) this.springContext
						.getBean(FunctionConfig.RETRIEVER_BEAN);
			}
			else
			{
				function = (Consumer<Message<ScheduledEvent>>) this.springContext.getBean(FunctionConfig.MOCK_BEAN);
				messageHeaders.put(Constants.VALIDATION, detail.get(Constants.VALIDATION));
				messageHeaders.put(Constants.EXCEPTION_VALIDATION, detail.get(Constants.EXCEPTION_VALIDATION));
			}

			// Create and send a message
			final Message<ScheduledEvent> message = new GenericMessage<>(argInput, messageHeaders);
			function.accept(message);

			return null;
		}
		finally
		{
			LOG.info("SolarStatsRetriever exiting");
		}
	}

	private String getAppVersion(final Context argContext)
	{
		final StringBuilder builder = new StringBuilder();
		builder.append(Constants.APP_VERSION);
		if (argContext != null && argContext.getFunctionVersion() != null)
		{
			builder.append(".");
			builder.append(argContext.getFunctionVersion());
		}

		return builder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws Exception
	{
		if (this.springContext != null)
		{
			this.springContext.close();
		}
	}

}
