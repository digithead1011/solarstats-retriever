/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.robcash.customer.model.Customer;
import org.robcash.enphase.v4.ReactiveEnphaseService;
import org.robcash.solarstats.retriever.monitoring.config.EnphaseMonitoringServiceTestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.enphaseenergy.api.v4.monitoring.model.Address;
import com.enphaseenergy.api.v4.monitoring.model.EnergyMetrics;
import com.enphaseenergy.api.v4.monitoring.model.SystemSummary;
import com.enphaseenergy.api.v4.monitoring.model.SystemsList;

import reactor.core.publisher.Mono;

/**
 * Unit tests for {@link EnphaseMonitoringService}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@SpringBootTest(classes = { EnphaseMonitoringServiceTestConfig.class })
@ActiveProfiles("local")
@EnableAutoConfiguration
class EnphaseMonitoringServiceTest
{
	// Instance to test.
	@Autowired
	private EnphaseMonitoringService instance;

	@Autowired
	private Customer customer;

	@Autowired
	private ReactiveEnphaseService mockService;

	/**
	 * Test method for
	 * {@link EnphaseMonitoringService#getSolarProductionStatistics(org.robcash.solarstats.retriever.customer.model.Customer, java.time.LocalDate)}.
	 */
	@Test
	public void testGetSolarProductionStatistics()
	{
		// Mock calls to underlying Enphase DAX to get production data
		final Address address = new Address()
				.state("NC")
				.country("US")
				.postalCode("28201");
		final SystemSummary systemSummary = new SystemSummary()
				.systemId(12345)
				.name("Sunshine")
				.publicName("Residential System")
				.timezone("US/Eastern")
				.address(address)
				.connectionType("wifi")
				.status("normal")
				.lastReportAt(1656359523)
				.lastEnergyAt(1656359326)
				.operationalAt(1569710308)
				.attachmentType("frame_mount");
		final SystemsList mockSystemsList = new SystemsList()
				.total(1)
				.currentPage(1)
				.size(10)
				.count(1)
				.items("systems")
				.systems(Collections.singletonList(systemSummary));
		Mockito.when(this.mockService.retrieveSystemsListFor(Mockito.anyString()))
				.thenReturn(Mono.just(mockSystemsList));
		final EnergyMetrics metrics = new EnergyMetrics()
				.systemId(12345)
				.startDate(LocalDate.of(2022, 6, 1))
				.production(Collections.singletonList(50000));
		Mockito.when(this.mockService.retrieveEnergyMetricsFor(Mockito.anyString(), Mockito.any(SystemSummary.class),
				Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Mono.just(metrics));

		// Before timestamp
		final Instant beforeTS = Instant.now();

		// Ask instance for production statistics
		final LocalDate asOfDate = LocalDate.ofInstant(Instant.now(), ZoneId.of("UTC")).minusDays(1);
		final List<org.robcash.solarstats.monitoring.model.EnergyMetrics> data = this.instance.getEnergyMetricsFor(
				this.customer, asOfDate).collectList().block();

		// After timestamp
		final Instant afterTS = Instant.now();

		// This should have generated 2 calls: one for systems list and one for systems detail
		Mockito.verify(this.mockService, Mockito.times(1)).retrieveSystemsListFor(Mockito.anyString());
		Mockito.verify(this.mockService, Mockito.times(1)).retrieveEnergyMetricsFor(Mockito.anyString(),
				Mockito.any(SystemSummary.class), Mockito.anyString(), Mockito.anyString());

		// Was list returned?
		Assertions.assertNotNull(data, "Data returned should never be null");
		Assertions.assertEquals(1, data.size(), "Number of systems doesn't match");
		final Instant energyDataAsOfDate = data.get(0).getSolarProduction().getAsOfDate().toInstant();
		Assertions.assertNotNull(energyDataAsOfDate, "Energy as-of date should not be null");
		Assertions.assertTrue(energyDataAsOfDate.isAfter(beforeTS), "As-of should be after start of test");
		Assertions.assertTrue(energyDataAsOfDate.isBefore(afterTS), "As-of should be before end of test");

	}

}
