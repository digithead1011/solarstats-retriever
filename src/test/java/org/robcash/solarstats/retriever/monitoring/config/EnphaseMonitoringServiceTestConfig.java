/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring.config;

import org.mockito.Mockito;
import org.robcash.customer.model.Customer;
import org.robcash.enphase.v4.ReactiveEnphaseService;
import org.robcash.solarstats.retriever.monitoring.EnphaseMonitoringService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Unit tests for {@link EnphaseMonitoringService}.
 *
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
@Configuration(proxyBeanMethods = false)
@Profile("local")
public class EnphaseMonitoringServiceTestConfig
{
	@Bean
	public ReactiveEnphaseService mockEnphaseDax()
	{
		return Mockito.mock(ReactiveEnphaseService.class);
	}

	@Bean
	public EnphaseMonitoringService enphaseMonitoringService(final ReactiveEnphaseService enphaseDax)
	{
		return new EnphaseMonitoringService(enphaseDax);
	}

	@Bean
	public Customer mockCustomer()
	{
		return new Customer()
				.id("testId")
				.name("Test Customer")
				.email("tester@bestcustomers.z");
	}

}
