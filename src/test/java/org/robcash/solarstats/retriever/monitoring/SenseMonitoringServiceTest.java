/*
 * Copyright (C) 2022 by Robert M Cash.
 * All Rights Reserved.
 */

package org.robcash.solarstats.retriever.monitoring;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.robcash.solarstats.monitoring.model.EnergyMetrics;
import org.robcash.solarstats.monitoring.model.EnergyUsage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Mono;

/**
 * @author <a href="mailto:robcash1011@gmail.com">Rob Cash</a>
 */
public class SenseMonitoringServiceTest
{
	/** Logger. */
	private static final Logger LOG = LoggerFactory.getLogger(SenseMonitoringServiceTest.class);

	private final WebClient apiClient;
	private final ObjectMapper mapper;

	public SenseMonitoringServiceTest()
	{
		this.mapper = new ObjectMapper();
		this.apiClient = WebClient.builder()
				.baseUrl("https://api.sense.com")
				.build();
	}

	@Test
	void testMath()
	{
		final double fromGrid = 50.30531849199906;
		final Double d = Double.valueOf(fromGrid * 1000);
		final long rounded = Math.round(d);
		final int from = Long.valueOf(rounded).intValue();
		Assertions.assertEquals(50305, from);
	}

	@Test
	void testJsonParsing() throws IOException
	{
		final String systemTimezoneAsString = "America/New_York";
		final String monitorId = "249049";
		final String authToken = "t1.50685.50214.f69b82bb19bc14b70f24c48ef74cc527fa2759a453f04480c63a3025224847bf";
		final LocalDate argAsOfDate = LocalDate.now();

		// The As of date is the date where this application is running, minus any offset days.
		// If the current time where the system (solar array) is located is before end of day on the as of day, then
		// the reporting period hasn't ended yet and we need to look at a prior day.

		// Get date and time where system is located
		final ZoneId systemZone = ZoneId.of(systemTimezoneAsString);
		final ZonedDateTime currentSystemTime = ZonedDateTime.now(systemZone);

		// Get as of date in timezone where system is located
		ZonedDateTime asOfTS = ZonedDateTime.of(argAsOfDate, LocalTime.MAX, systemZone);

		while (currentSystemTime.isBefore(asOfTS))
		{
			// System time is before proposed end of period.
			// Energy production not done for that time period, so back up a day.
			asOfTS = asOfTS.minusDays(1);
		}
		final ZonedDateTime periodStartTS = asOfTS.truncatedTo(ChronoUnit.DAYS);

		// Enphase allows API consumers to pass an end-of-period time expressed as time in millis since the Unix epoch
		final String periodStart = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(periodStartTS);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Calling Sense to retrieve usage data for monitor {} as of {} at end of day",
					monitorId, periodStartTS.format(DateTimeFormatter.ISO_LOCAL_DATE));
		}

		final Mono<EnergyMetrics> promise = this.apiClient
				.get()
				.uri(builder -> builder.path("/apiservice/api/v1/app/history/trends")
						.queryParam("monitor_id", monitorId)
						.queryParam("scale", "DAY")
						.queryParam("start", periodStart)
						.build())
				.headers(h -> h.setBearerAuth(authToken))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(String.class)
				.map(s -> {
					try
					{
						return this.mapper.readTree(s);
					}
					catch (final JsonProcessingException ex)
					{
						throw new RuntimeException("Failed to read JSON response from Sense API call", ex);
					}
				})
				.map(json -> new EnergyMetrics()
						.customerId("12345")
						.usage(new EnergyUsage()
								.dataSource("Sense")
								.energyFromGrid(
										(int) Math.round(json.path("from_grid").asDouble(0) * 1000))
								.energyToGrid(
										(int) Math.round(json.path("to_grid").asDouble(0) * 1000))
								.asOfDate(new Date())));

		final EnergyMetrics metrics = promise.block();
		// Assertions.assertEquals(50305, metrics.getUsage().getEnergyFromGrid(), "from_grid doesn't match");
		// Assertions.assertEquals(47394, metrics.getUsage().getEnergyToGrid(), "to_grid doesn't match");
	}
}
